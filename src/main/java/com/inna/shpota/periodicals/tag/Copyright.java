package com.inna.shpota.periodicals.tag;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

public class Copyright extends SimpleTagSupport {
    @Override
    public void doTag() throws IOException {
        JspWriter writer = getJspContext().getOut();
        writer.write("<div id=\"footer\">");
        writer.write("&copy; 2018 Inna Shpota");
        writer.write("</div>");
    }
}
